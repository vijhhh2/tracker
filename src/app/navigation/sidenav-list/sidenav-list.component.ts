import { Component, OnInit, Output, EventEmitter, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';

import { AuthService } from '../../auth/auth.service';
import { Subscription } from 'rxjs/Subscription';
import * as fromRoot from '../../app.reducer';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-sidenav-list',
  templateUrl: './sidenav-list.component.html',
  styleUrls: ['./sidenav-list.component.css']
})
export class SidenavListComponent implements OnInit, OnDestroy {
  isAuth$: Observable<boolean>;
  subscription: Subscription;

  @Output() closeSideNav = new EventEmitter<void>();

  constructor(private _authService: AuthService, private _store: Store<fromRoot.State>) { }

  ngOnInit() {
    this.isAuth$ = this._store.select(fromRoot.getIsAuthenticated);
  }

  onClose() {
    this.closeSideNav.emit();
  }
  onLogout() {
    this.onClose();
    this._authService.logout();
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
