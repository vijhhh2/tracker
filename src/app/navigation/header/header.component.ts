import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Store } from '@ngrx/store';


import { AuthService } from '../../auth/auth.service';
import * as fromRoot from '../../app.reducer';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  subscription: Subscription;

 @Output() sidenavToggle = new EventEmitter<void>();

  constructor(private _authService: AuthService, private _store: Store<fromRoot.State>) { }
  isAuth$: Observable<boolean>;

  ngOnInit() {

     this.isAuth$ = this._store.select(fromRoot.getIsAuthenticated);
  }

  onToggleSidenav() {

    this.sidenavToggle.emit();

  }
  onLogout() {
    this._authService.logout();
  }


}
