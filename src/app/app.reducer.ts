import * as fromUI from './shared/ui.reducer';
import * as fromAuth from './auth/auth.reducer';
import { ActionReducerMap, createFeatureSelector, createSelector } from '@ngrx/store';

export interface State {
  ui: fromUI.State;
  auth: fromAuth.State;
}

export const reducers: ActionReducerMap<State> = {
  ui : fromUI.Uireducer,
  auth : fromAuth.AuthReducer
};

export const getUiState = createFeatureSelector<fromUI.State>('ui');
export const getloading = createSelector(getUiState, fromUI.getIsloading);


export const getAuthState = createFeatureSelector<fromAuth.State>('auth');
export const getIsAuthenticated = createSelector(getAuthState, fromAuth.getIsAuthenticate);
