import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';

import { AuthService } from '../auth.service';
import { UIService } from '../../shared/ui.service';
import { Subscription } from 'rxjs/Subscription';
import * as fromRoot from '../../app.reducer';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  myGroup: FormGroup;

  // tslint:disable-next-line:max-line-length
  constructor(private formBuilder: FormBuilder, private _authService: AuthService, private _UIS: UIService, private _store: Store<fromRoot.State>) { }
 IsLoading$: Observable<boolean>;
 private subscription: Subscription;
  ngOnInit() {
    this.myGroup = this.formBuilder.group({
      'email': this.formBuilder.control('', [Validators.required, Validators.email]),
      'password': this.formBuilder.control('', Validators.required)
    });
    // this.subscription = this._UIS.lodingStateChanged
    // .subscribe(state => this.IsLoading = state);
    this.IsLoading$ = this._store.select(fromRoot.getloading);
    // this.IsLoading$.subscribe(state => console.log(state));
  }
  onSubmit() {
    this._authService.login({
      email: this.myGroup.value.email,
      password: this.myGroup.value.password
    });
  }

}
