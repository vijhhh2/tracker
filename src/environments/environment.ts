// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyCKhMgZ0GT9sKwTbuJ8ZyVXixNFL0Yqyc4',
    authDomain: 'tracker-abe7c.firebaseapp.com',
    databaseURL: 'https://tracker-abe7c.firebaseio.com',
    projectId: 'tracker-abe7c',
    storageBucket: 'tracker-abe7c.appspot.com',
    messagingSenderId: '948333752069'
  }
};
