import { Exercise } from './exercise.model';
import * as fromRoot from '../app.reducer';
import { TrainingActions, SET_AVAILABLE_EXERCISES, SET_FINISHED_EXERCISES, START_EXERCISE, STOP_EXERCISE } from './training.actions';
import { createFeatureSelector, createSelector } from '@ngrx/store';

export interface TrainingState {
  avialbleExercises: Exercise[];
  finshedExercises: Exercise[];
  activeExercises: Exercise;
}

export interface State extends fromRoot.State {
  training: TrainingState;
}


export const intialState: TrainingState = {
  avialbleExercises : [],
  finshedExercises : [],
  activeExercises : null
};


export function TrainingReducer (state = intialState, action: TrainingActions) {

  switch (action.type) {
    case SET_AVAILABLE_EXERCISES:
      return {
        ...state,
        avialbleExercises : action.payload
      };

    case SET_FINISHED_EXERCISES:
    return {
      ...state,
      finshedExercises : action.payload
    };
    case START_EXERCISE:
    return {
      ...state,
      activeExercises : {...state.avialbleExercises.find(ex => ex.id === action.payload)}
    };
    case STOP_EXERCISE:
    return {
      ...state,
      activeExercises : null
    };

    default:
    return state;
  }
}

export const getTrainingState = createFeatureSelector('training');

export const getAvialbleExercises = createSelector(getTrainingState, (state: TrainingState) => state.avialbleExercises);
export const getFinshedExercises = createSelector(getTrainingState, (state: TrainingState) => state.finshedExercises);
export const getActiveExercises = createSelector(getTrainingState, (state: TrainingState) => state.activeExercises);
export const getIsAvialbleExercises = createSelector(getTrainingState, (state: TrainingState) => state.activeExercises != null);
