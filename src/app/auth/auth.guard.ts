import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, CanLoad, Route } from '@angular/router';
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { take } from 'rxjs/operators';

import { AuthService } from './auth.service';
import { Observable } from 'rxjs/Observable';
import * as fromRoot from '../app.reducer';


@Injectable()

export class AuthGuard implements CanActivate, CanLoad {


  constructor(private _authService: AuthService, private _router: Router, private _store: Store<fromRoot.State>) {}
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this._store.select(fromRoot.getIsAuthenticated).pipe(take(1));
  }
  canLoad(route: Route) {
    return this._store.select(fromRoot.getIsAuthenticated).pipe(take(1));
  }
}
