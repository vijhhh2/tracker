import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { MatSnackBar } from '@angular/material';


@Injectable()

export class UIService {

  lodingStateChanged = new Subject<boolean>();

  constructor( private _snackbar: MatSnackBar) {}

  showSnackbar(message, action, duration) {
    this._snackbar.open(message, action, {
      duration: duration
    });
  }
}
