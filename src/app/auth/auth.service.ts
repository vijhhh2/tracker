import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFireAuth } from 'angularfire2/auth';
import { Store } from '@ngrx/store';

import { User } from './user.model';
import { AuthData } from './auth-data.model';
import { TrainingService } from '../training/training.service';
import { MatSnackBar } from '@angular/material';
import { UIService } from '../shared/ui.service';
import * as fromRoot from '../app.reducer';
import * as UI from '../shared/ui.actions';
import * as AUTH from '../auth/auth.actions';

@Injectable()
export class AuthService {
  private isAuthenticated = false;

  constructor(
    private _router: Router,
    private _afAuth: AngularFireAuth,
    private _trainingService: TrainingService,
    private _UIser: UIService,
    private _store: Store<fromRoot.State>
  ) {}

  initAuthListner() {
    this._afAuth.authState.subscribe(user => {
      if (user) {
        this._store.dispatch(new AUTH.SetAuthentication());
        this._router.navigate(['/training']);
      } else {
        this._store.dispatch(new AUTH.SetAuthentication());
        this._trainingService.cancelSubs();
        this._router.navigate(['/login']);
      }
    });
  }
  registerUser(authData: AuthData) {
    // this._UIser.lodingStateChanged.next(true);
    this._store.dispatch(new UI.StartLoading());
    this._afAuth.auth
      .createUserWithEmailAndPassword(authData.email, authData.password)
      .then(res => {
        // this._UIser.lodingStateChanged.next(false);
        this._store.dispatch(new UI.StopLoading());
      })
      .catch(err => {
        // this._UIser.lodingStateChanged.next(false);
        this._store.dispatch(new UI.StopLoading());
        this._UIser.showSnackbar(err.message, null, 3000);
      });
  }

  login(authData: AuthData) {
    // this._UIser.lodingStateChanged.next(true);
    this._store.dispatch(new UI.StartLoading());
    this._afAuth.auth
      .signInWithEmailAndPassword(authData.email, authData.password)
      .then(res => {
        // this._UIser.lodingStateChanged.next(false);
        this._store.dispatch(new UI.StopLoading());
      })
      .catch(err => {
        // this._UIser.lodingStateChanged.next(false);
        this._store.dispatch(new UI.StopLoading());
        this._UIser.showSnackbar(err.message, null, 3000);
      });
  }

  logout() {
    this._afAuth.auth.signOut();
  }
}
