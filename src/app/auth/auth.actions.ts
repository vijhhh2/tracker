import { Action } from '@ngrx/store';

export const SET_AUTHENTICATED = '[Auth] Set Authentication';
export const SET_UNAUTHENTICATED = '[Auth] Set Unauthentication';

export class SetAuthentication implements Action {
 readonly type = SET_AUTHENTICATED;
}
export class SetUnAuthentication implements Action {
 readonly type = SET_UNAUTHENTICATED;
}


export type AuthActions = SetAuthentication | SetUnAuthentication;
