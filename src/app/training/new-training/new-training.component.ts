import {
  Component,
  OnInit,
  EventEmitter,
  Output
} from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import { Observable } from 'rxjs/observable';
import { map } from 'rxjs/operators';

import { TrainingService } from '../training.service';
import { Exercise } from '../exercise.model';
import { NgForm } from '@angular/forms';
import { Store } from '@ngrx/store';

import * as fromRoot from '../../app.reducer';
import * as fromTraining from '../training.reducer';
import { UIService } from '../../shared/ui.service';

@Component({
  selector: 'app-new-training',
  templateUrl: './new-training.component.html',
  styleUrls: ['./new-training.component.css']
})
export class NewTrainingComponent implements OnInit  {
  @Output() newTraining = new EventEmitter<void>();
  IsLoading$: Observable<boolean>;

  exercises$: Observable<Exercise[]>;

  constructor(
    private _trainingService: TrainingService,
    private _UIS: UIService,
    private _store: Store<fromTraining.State>
  ) {}

  ngOnInit() {
    this.exercises$ = this._store.select(fromTraining.getAvialbleExercises);
    this.fethExercises();
    this.IsLoading$ = this._store.select(fromRoot.getloading);
  }

  fethExercises() {
    this._trainingService.fetchAvailbleExercises();
  }

  onSubmit(form: NgForm) {
    this._trainingService.startExercise(form.value.exercise);
  }


}
