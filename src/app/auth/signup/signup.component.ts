import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Store } from '@ngrx/store';

import { AuthService } from '../auth.service';
import { Subscription } from 'rxjs/Subscription';
import { UIService } from '../../shared/ui.service';
import * as fromRoot from '../../app.reducer';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  IsLoading$: Observable<boolean>;
  subscription: Subscription;
  maxDate;
  minDate;
  constructor(private _authService: AuthService, private _UIS: UIService, private _store: Store<fromRoot.State>) { }

  ngOnInit() {
    this.maxDate = new Date();
    this.minDate = new Date();
    this.maxDate.setFullYear(this.maxDate.getFullYear() - 18);
    this.minDate.setFullYear(this.minDate.getFullYear() - 53);
    // this.subscription = this._UIS.lodingStateChanged
    // .subscribe(state => this.IsLoading = state);
   this.IsLoading$ =  this._store.select(fromRoot.getloading);
  }

  onSubmit(form: NgForm) {
    this._authService.registerUser({
      email: form.value.email,
      password: form.value.password
    });
  }


}
