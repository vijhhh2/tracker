import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Store } from '@ngrx/store';
import { take } from 'rxjs/operators';

import { StopTrainingComponent } from './stop-training.component';
import { TrainingService } from '../training.service';
import * as fromTraining from '../training.reducer';

@Component({
  selector: 'app-current-training',
  templateUrl: './current-training.component.html',
  styleUrls: ['./current-training.component.css']
})
export class CurrentTrainingComponent implements OnInit {
  progress = 0;
  timer;
  constructor(
    private _dailouge: MatDialog,
    private _trainingService: TrainingService,
    private _store: Store<fromTraining.State>
  ) {}

  ngOnInit() {
    this.startOrStopTimer();
  }

  startOrStopTimer() {
    this._store.select(fromTraining.getActiveExercises).pipe(take(1)).subscribe((ex) => {
      const step =
      ex.duration / 100 * 1000;
    this.timer = setInterval(() => {
      this.progress = this.progress + 1;
      if (this.progress >= 100) {
        this._trainingService.completeExercise();
        clearInterval(this.timer);
      }
    }, step);
    });
  }

  onStop() {
    clearInterval(this.timer);
    const dailogRef = this._dailouge.open(StopTrainingComponent, {
      data: {
        progress: this.progress
      }
    });
    dailogRef.afterClosed().subscribe(res => {
      if (res) {
        this._trainingService.cancelExercise(this.progress);
      } else {
        this.startOrStopTimer();
      }
    });
  }
}
