import { UIActions, START_LOADING, STOP_LOADING } from './ui.actions';

export interface State {
  isLoading: boolean;
}


export const intialState: State = {
  isLoading: false
};


export function Uireducer (state = intialState, action: UIActions) {

  switch (action.type) {
    case START_LOADING:
      return {
        isLoading : true
      };

    case STOP_LOADING:
    return {
      isLoading : false
    };

    default:
    return state;
  }
}
// tslint:disable-next-line:no-shadowed-variable
export const getIsloading = (state: State) => {
  return state.isLoading;
};
