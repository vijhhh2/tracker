import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { AngularFirestore } from 'angularfire2/firestore';
import { Subscription } from 'rxjs/Subscription';
import { Store } from '@ngrx/store';

import { Exercise } from './exercise.model';
import { UIService } from '../shared/ui.service';
import * as fromTraining from './training.reducer';
import * as UI from '../shared/ui.actions';
import * as Training from './training.actions';
import { take } from 'rxjs/operators';

@Injectable()

export class TrainingService {
  private availbeExercise: Exercise[] = [];
  private runningExercise: Exercise;
  private fbSubs: Subscription[] = [];
  exerciseChanged = new Subject<Exercise>();
  exercisesChanged = new Subject<Exercise[]>();
  finshedExercisesChanged = new Subject<Exercise[]>();

  constructor(private _db: AngularFirestore, private _UIS: UIService, private _store: Store<fromTraining.State>) {}
  fetchAvailbleExercises() {
    // this._UIS.lodingStateChanged.next(true);
    this._store.dispatch(new UI.StartLoading());
    this.fbSubs.push(this._db.collection('availableExercises').snapshotChanges()
    .map((docArray) => {
     return docArray.map((doc) => {
      return {
        id: doc.payload.doc.id,
        name: doc.payload.doc.data().name,
        duration: doc.payload.doc.data().duration,
        calories: doc.payload.doc.data().calories,
      };
      });
    }).subscribe((exercises: Exercise[]) => {
      // this._UIS.lodingStateChanged.next(false);
      this._store.dispatch(new UI.StopLoading());
      this._store.dispatch(new Training.SetAvailableExercises(exercises));
    }, error => {
      // this._UIS.lodingStateChanged.next(false);
      this._store.dispatch(new UI.StopLoading());
      this._UIS.showSnackbar('Cant fetch Exercises', null, 3000);
      this._store.dispatch(new Training.SetAvailableExercises(null));
    }));
  }

  startExercise(cid: string) {
    // this._db.doc('availableExercises/' + cid).update({lastSeen: new Date().toString()});
    this._store.dispatch(new Training.StartExercise(cid));
  }

  completeExercise() {
    this._store.select(fromTraining.getActiveExercises).pipe(take(1)).subscribe((ex) => {
      this.addDataToDatabse({...ex, date: new Date(), state: 'completed'});
    });
    this._store.dispatch(new Training.StopExercise());
  }
  cancelExercise(progress: number) {
    this._store.select(fromTraining.getActiveExercises).pipe(take(1)).subscribe((ex) => {
      this.addDataToDatabse({...this.runningExercise,
        duration: ex.duration * (progress / 100),
        calories: ex.calories * (progress / 100),
      date: new Date(), state: 'cancelled'});
      this._store.dispatch(new Training.StopExercise());
    });
  }


  fetchCompletedOrCancelledExercises() {
    this.fbSubs.push(this._db.collection('finshedExercise').valueChanges()
    .subscribe((exercises: Exercise[]) => {
      this._store.dispatch(new Training.SetFinshedExercises(exercises));
    }));
  }
  cancelSubs() {
    this.fbSubs.forEach(sub => sub.unsubscribe());
  }
  private addDataToDatabse(exercise: Exercise) {
    this._db.collection('finshedExercise').add(exercise);
  }


}
