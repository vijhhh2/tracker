import { AuthActions, SET_AUTHENTICATED, SET_UNAUTHENTICATED } from '../auth/auth.actions';

export interface State {
  isAuthenticated: boolean;
}

export const intialState: State = {
  isAuthenticated: false
};

export function AuthReducer (state = intialState, action: AuthActions) {
  switch (action.type) {
    case SET_AUTHENTICATED:
    return {
      isAuthenticated: true
    };
    case SET_UNAUTHENTICATED:
    return {
      isAuthenticated: false
    };
    default: return state;
  }
}

export const getIsAuthenticate = (state: State) => state.isAuthenticated;
